from django.urls import path
from . import views


urlpatterns = [
    path('employee/', views.EmployeeListCreatView.as_view()),
    path('employee/<int:pk>/', views.EmployeeDetail.as_view()),
]
