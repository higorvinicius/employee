from rest_framework import generics
from .models import Employee
from .serializers import CreateEmployeeSerializer


class EmployeeListCreatView(generics.ListCreateAPIView):
    queryset = Employee.objects.all()
    serializer_class = CreateEmployeeSerializer


class EmployeeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Employee.objects.all()
    serializer_class = CreateEmployeeSerializer
