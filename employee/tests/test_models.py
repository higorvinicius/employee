from django.db import IntegrityError
from django.test import TestCase
from employee.models import Employee


class EmployeeModelTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):

        Employee.objects.create(
            name='Arnaldo Pereira',
            email='arnaldo@luizalabs.com',
            department='Mobile'
        )

    def test_valid_employee(self):
        data = {
            'name': 'Arnaldo Pereira',
            'email': 'arnaldo1@luizalabs.com',
            'department': 'Mobile'
        }
        employee = Employee.objects.create(**data)
        self.assertEqual(employee.name, data['name'])
        self.assertEqual(employee.email, data['email'])
        self.assertEqual(employee.department, data['department'])

    def test_duplicate_employee(self):
        data = {
            'name': 'Arnaldo Pereira',
            'email': 'arnaldo2@luizalabs.com',
            'department': 'Mobile'
        }
        Employee.objects.create(**data)
        with self.assertRaises(IntegrityError):
            Employee.objects.create(**data)

    def test_invalid_data(self):
        data = {
            'name': 'Arnaldo Pereira',
            'email': None,
            'department': None
        }
        with self.assertRaises(IntegrityError):
            Employee.objects.create(**data)
