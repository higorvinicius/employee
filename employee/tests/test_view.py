from django.test import TestCase
from employee.models import Employee
from employee.serializers import CreateEmployeeSerializer


class EmployeeListViewTest(TestCase):

    def setUp(self):
        self.employee_attributes = {
            'name': 'Thiago Catoto',
            'email': 'catoto@luizalabs.com',
            'department': 'Mobile'
        }

        self.serializer_data = {
            'name': 'Thiago Catoto',
            'email': 'catoto@luizalabs.com',
            'department': 'Mobile'
        }

        self.employee = Employee.objects.create(**self.employee_attributes)
        self.serializer = CreateEmployeeSerializer(instance=self.employee)

    def test_contains_expected_fields(self):
        data = self.serializer.data
        self.assertEqual(
            set(data.keys()),
            set(['name', 'email', 'department'])
        )

    def test_fields_content(self):
        data = self.serializer.data

        self.assertEqual(data['name'], self.employee_attributes['name'])
        self.assertEqual(data['email'], self.employee_attributes['email'])
        self.assertEqual(
            data['department'],
            self.employee_attributes['department']
        )
