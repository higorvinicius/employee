from django.db import models


class Employee(models.Model):

    name = models.CharField('Name', max_length=100)
    email = models.EmailField('E-mail', max_length=70, unique=True)
    department = models.CharField('Department', max_length=100)

    def __str__(self):
        return ' %s - %s - %s ' % (self.name, self.email, self.department)

    class Meta:
        verbose_name = 'Employee'
        verbose_name_plural = 'Employees'
        ordering = ['name']
