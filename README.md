# Luizalabs Employee Manager
API created to manage employees information , such as name, e-mail and department.

### clone the repository

```
git clone https://github.com/HigorMonteiro/employees.git
cd employee/
```
### Ini file
Now create a settings.ini next to your configuration module, see example:
```
[settings]
DEBUG=True
TEMPLATE_DEBUG=%(DEBUG)s
SECRET_KEY=ARANDOMSECRETKEY
DATABASE_URL=postgres://myuser:mypassword@myhost/mydatabase

```
*Note*: Since ``ConfigParser`` supports *string interpolation*, to represent the character ``%`` you need to escape it as ``%%``.
You can read more about Python Decouple at: https://github.com/henriquebastos/python-decouple

### Step by step to start the application
```
pip install -U pip
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
python manage.py migrate
python manage.py test
python manage.py runserver
```

### Running with Docker:
[Install docker] (https://docs.docker.com/install/)
[Install the docker-compose] (https://docs.docker.com/compose/install/)

```
docker-compose build
docker-compose run web python manage.py migrate
docker-compose run web python manage.py createsuperuser
docker-compose up
```
Now let's testing the API

### POST
```
curl -H "Content-Type: application/json" -d '{"name": "Arnaldo Pereira", "email": "arnaldo@luizalabs.com", "department": "Architecture"}' http://localhost:8000/employee/
curl -H "Content-Type: application/json" -d '{"name": "Renato Pedigoni", "email": "renato@luizalabs.com", "department": "E-commerce"}' http://localhost:8000/employee/
curl -H "Content-Type: application/json" -d '{"name": "Thiago Catoto", "email": "catoto@luizalabs.com", "department": "Mobile"}' http://localhost:8000/employee/
```

### GET
```
curl -H "Content-Type: application/javascript" http://localhost:8000/employee/
```
